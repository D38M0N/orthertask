import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class GreetingTest {
    Greeting greeting;

    @BeforeEach
    void setUp() {
        greeting = new Greeting();
    }

    @ParameterizedTest
    @MethodSource("metodSourceArray")
    void name(String mesagee, String[] name) {
        assertEquals(mesagee, greeting.arrayGreet(name));

    }

    private static Stream<Arguments> metodSourceArray() {
        return Stream.of(
                Arguments.of("Hello, Bob.", new String[]{"Bob"}),
                Arguments.of("Hello, my Friend.", null),
                Arguments.of("HELLO, JERRY.", new String[]{"JERRY"}),
                Arguments.of("Hello, Janek, and Staszek.", new String[]{"Janek", "Staszek"}),
                Arguments.of("Hello, Amy, Brian, and Charlotte.", new String[]{"Amy","Brian","Charlotte"}),
                Arguments.of("Hello, Amy, and Charlotte. AND HELLO BRIAN!", new String[]{"Amy", "BRIAN", "Charlotte"}),
                Arguments.of("Hello, Bob, Charlie, and Dianne.", new String[]{"Bob", "Charlie, Dianne"})
        );

    }
}




