import java.util.ArrayList;
import java.util.List;

public class FizzBuzz {

    List<String> getNumb(int number) {
        List<String> list = new ArrayList<String>();
        for (int i = 1; i <= number; i++) {
            if (i % 3 == 0 && i % 5 != 0) {
                list.add("Fizz");
            } else if (i % 5 == 0 && i % 3 != 0) {
                list.add("Buzz");

            } else if (i % 5 == 0 && i % 3 == 0) {
                list.add("FizzBuzz");

            } else {
                list.add(String.valueOf(i));
            }
        }
        return list;
    }
}
