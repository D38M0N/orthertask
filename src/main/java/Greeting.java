import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Greeting {
    String arrayGreet(String[] arrayString) {
        ArrayList<String> arrayList = new ArrayList<>();
        StringBuilder sb = new StringBuilder("Hello,");
        if (isNull(arrayString)) {
            return sb.append(" my Friend.").toString();
        } else {
            splitToList(arrayString, arrayList);
            String upperResult = allUpper(arrayList);
            if (upperResult != null) return upperResult;
            createStringWithList(arrayList, sb);
            searchUpperString(arrayList, sb);
        }
        return sb.toString();
    }

    private String allUpper(ArrayList<String> arrayList) {
        for (int i = 0; i < arrayList.size(); i++) {
            if (isUpperFirstName(arrayList, i)) {
                return "HELLO, " + arrayList.get(i) + ".";
            }
        }
        return null;
    }

    private void searchUpperString(ArrayList<String> arrayList, StringBuilder sb) {
        for (int i = 0; i < arrayList.size(); i++) {
            if (arrayList.get(i).toUpperCase().equals(arrayList.get(i))) {
                sb.append(" AND HELLO " + arrayList.get(i) + "!");
            }
        }
    }

    private void createStringWithList(ArrayList<String> arrayList, StringBuilder sb) {
        for (int i = 0; i < arrayList.size(); i++) {

            if (isNotUpper(arrayList, i)) {
                sb.append(" " + arrayList.get(i).replace(" ", ""));

                if (isNotLast(arrayList, i)) {
                    sb.append(",");
                } else {
                    sb.append(".");
                }
            }
            if (i == arrayList.size() - 2) {
                sb.append(" and");
            }
        }

    }

    private void splitToList(String[] arrayString, ArrayList<String> arrayList) {
        for (int i = 0; i < arrayString.length; i++) {
            List<String> splitList = Arrays.asList(arrayString[i].split(","));
            arrayList.addAll(splitList);
        }
    }

    private boolean isNull(String[] arrayString) {
        return arrayString == null;
    }

    private boolean isNotLast(ArrayList<String> arrayList, int i) {
        return i != arrayList.size() - 1;
    }

    private boolean isNotUpper(ArrayList<String> arrayList, int i) {
        return !arrayList.get(i).toUpperCase().equals(arrayList.get(i));
    }

    private boolean isUpperFirstName(ArrayList<String> arrayList, int i) {
        return arrayList.get(i).toUpperCase().equals(arrayList.get(i)) && arrayList.size() == 1;
    }
}
